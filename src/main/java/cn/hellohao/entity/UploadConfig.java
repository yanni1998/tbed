package cn.hellohao.entity;

import lombok.Data;

@Data
public class UploadConfig {
    private String suffix;
    private Integer filesizetourists;
    private Integer filesizeuser;
    private Integer imgcounttourists;
    private Integer imgcountuser;
    private Integer urltype;
    private Integer isupdate;
    private Integer api;
    private Integer visitormemory;
    private Integer usermemory;
    private String blacklist;


    public UploadConfig() {
    }

}

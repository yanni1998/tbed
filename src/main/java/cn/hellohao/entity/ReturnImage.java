package cn.hellohao.entity;

import lombok.Data;

/**
 * @author Hellohao
 * @version 1.0
 * @date 2019-07-22 11:35
 */
@Data
public class ReturnImage {
    private String imgurl;
    private String imgname;

    public ReturnImage() {
    }

}

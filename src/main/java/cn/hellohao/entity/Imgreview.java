package cn.hellohao.entity;

import lombok.Data;

@Data
public class Imgreview {
    private Integer id;

    private String appId;

    private String apiKey;

    private String secretKey;

    private Integer using;

    private Integer count;

    public Imgreview() {
    }

}
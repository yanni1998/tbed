package cn.hellohao.entity;

import lombok.Data;

@Data
public class EmailConfig {
    private Integer id ;
    private String emails;
    private String emailkey;
    private String emailurl;
    private String port;
    private String emailname;
    private Integer using ;

    public EmailConfig() {
    }

}

package cn.hellohao.entity;

import lombok.Data;

/**
 * @author Hellohao
 * @version 1.0
 * @date 2019/8/19 16:01
 */
@Data
public class UserGroup {
    private Integer id;
    private Integer userid;
    private Integer groupid;

}

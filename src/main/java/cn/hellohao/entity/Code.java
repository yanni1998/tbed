package cn.hellohao.entity;

import lombok.Data;

/**
 * @author Hellohao
 * @version 1.0
 * @date 2019-08-11 14:09
 */
@Data
public class Code {
    private Integer id;
    private Integer value;
    private String code;

    public Code() {
    }

}

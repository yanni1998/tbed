package cn.hellohao.entity;

import lombok.Data;

@Data
public class User {

    private Integer id;
    //@NotBlank(message = "用户名不能为空")
   // @Length(min = 6, max = 20, message = "用户名需要为 6 - 20 个字符")
    private String username;
    //@NotBlank(message = "密码不能为空")
    private String password;
   // @NotBlank(message = "邮箱不能为空")
    //@Email(message = "邮箱格式不正确")
    private String email;
    private String birthder;
    private Integer level;
    private String uid;
    private Integer isok;
    private  Integer memory;
    private Integer groupid;


    public User( ) {
        super();
    }

}

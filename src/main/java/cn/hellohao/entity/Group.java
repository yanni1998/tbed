package cn.hellohao.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

/**
 * @author Hellohao
 * @version 1.0
 * @date 2019/8/19 15:59
 */
@Data
public class Group {
    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;
    private String groupname;
    private Integer keyid;

}

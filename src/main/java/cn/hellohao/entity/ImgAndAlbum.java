package cn.hellohao.entity;

import lombok.Data;

/**
 * @author Hellohao
 * @version 1.0
 * @date 2019-12-18 22:12
 */
@Data
public class ImgAndAlbum {
    private String imgname;
    private String albumkey;

    public ImgAndAlbum() {
    }


}

package cn.hellohao.entity;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

@Data
public class Images {
    // 默认的时间字符串格式

    //id, imgname, imgurl, userid
    private Integer id;
    private String imgname;
    private String imgurl;
    private Integer userid;
    private Integer sizes = 0;
    private String abnormal;
    private Integer source;
    private Integer imgtype;
    private String updatetime;
    private String username;
    private Integer storageType;
    private String starttime;
    private String stoptime;
    @Length(min = 1, max = 100, message = "图片描述不得超过100个字符")
    private String explains;
    private String md5key;
    //Album
    @NotBlank(message = "画廊标题不能为空")
    @Length(min = 1, max = 50, message = "画廊标题不得超过50个字符")
    private String albumtitle;
    @Length(min = 0, max = 10, message = "画廊密码不能超过10个字符")
    private String password;
    private Integer selecttype;

    public Images() {
        super();
    }


}
	
		


package cn.hellohao.entity;

import lombok.Data;

/**
 * @author Hellohao
 * @version 1.0
 * @date 2019/8/15 13:27
 */
@Data
public class SysConfig {
    private Integer id;
    private Integer register;

    public SysConfig() {
    }

}

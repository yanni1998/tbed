package cn.hellohao.entity;

import lombok.Data;

/**
 * @author yanni
 */
@Data
public class Keys {
    private Integer id;
    private String accessKey;
    private String accessSecret;
    private String endpoint;
    private String bucketname;
    private String requestAddress;
    private Integer storageType;

    public Keys() {
        super();
    }

}

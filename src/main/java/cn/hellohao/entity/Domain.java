package cn.hellohao.entity;

import lombok.Data;

/**
 * @author Hellohao
 * @version 1.0
 * @date 2019/8/21 9:43
 */
@Data
public class Domain {
    private Integer id;
    private String domain;
    private String code;

    public Domain() {
    }

}

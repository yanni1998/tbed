package cn.hellohao.entity;

import lombok.Data;

/**
 * @author Hellohao
 * @version 1.0
 * @date 2019-12-18 22:13
 */
@Data
public class Album {
    private String  albumkey;
    private String albumtitle;
    private String createdate;
    private String password;
    private Integer userid;
    private String username;

}

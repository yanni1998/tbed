package cn.hellohao.service;

import cn.hellohao.entity.Group;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Hellohao
 * @version 1.0
 * @date 2019/8/19 16:29
 */
@Service
public interface GroupService extends IService<Group> {
    List<Group> grouplist();

    Group idgrouplist(Integer id);

    Integer delegroup(Integer id);

}

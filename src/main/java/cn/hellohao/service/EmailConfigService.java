package cn.hellohao.service;

import cn.hellohao.entity.EmailConfig;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.stereotype.Service;

@Service
public interface EmailConfigService extends IService<EmailConfig> {
    /**
     *  获取邮件
     * @return email
     */
    EmailConfig getemail();
}

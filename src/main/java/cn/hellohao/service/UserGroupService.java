package cn.hellohao.service;

import cn.hellohao.entity.UserGroup;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.stereotype.Service;

/**
 * @author Hellohao
 * @version 1.0
 * @date 2019/8/20 14:12
 */

public interface UserGroupService extends IService<UserGroup> {
    Integer updateusergroupdefault(Integer groupid);
}

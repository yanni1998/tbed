package cn.hellohao.service;

import cn.hellohao.entity.Notice;
import com.baomidou.mybatisplus.extension.service.IService;

public interface NoticeService extends IService<Notice> {

    String getNotice();
}

package cn.hellohao.service;


import cn.hellohao.entity.Keys;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

public interface KeysService extends IService<Keys> {
    /**
     * 查询密钥
     * @param storageType 检验码
     * @return keys
     */
    Keys selectKeys(Integer storageType);


    List<Keys> getKeys();
}

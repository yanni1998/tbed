package cn.hellohao.service;

import cn.hellohao.entity.Album;
import cn.hellohao.entity.ImgAndAlbum;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Hellohao
 * @version 1.0
 * @date 2019-12-18 22:15
 */
@Service
public interface AlbumService extends IService<Album> {

    Album selectAlbum(Album album);

    Integer addAlbum(Album album);

    Integer deleteAlbum(String albumkey);
    Integer addAlbumForImgAndAlbumMapper(ImgAndAlbum imgAndAlbum);
    List<Album> selectAlbumURLList(Album album);
}

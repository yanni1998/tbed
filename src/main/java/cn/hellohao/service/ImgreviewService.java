package cn.hellohao.service;

import cn.hellohao.entity.Imgreview;

public interface ImgreviewService {

    /**
     * 删除图片
     * @param id 图片id
     * @return true false
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * 插入
     * @param record 记录
     * @return true or false
     */
    int insert(Imgreview record);

    int insertSelective(Imgreview record);

    Imgreview selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Imgreview record);

    int updateByPrimaryKey(Imgreview record);

}

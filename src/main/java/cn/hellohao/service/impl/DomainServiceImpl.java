package cn.hellohao.service.impl;

import cn.hellohao.entity.Domain;
import cn.hellohao.mapper.DomainMapper;
import cn.hellohao.service.DomainService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author Hellohao
 * @version 1.0
 * @date 2019/8/21 9:51
 */
@Service
public class DomainServiceImpl extends ServiceImpl<DomainMapper,Domain> implements DomainService {
    @Resource
    private  DomainMapper domainMapper;

    @Override
    public Integer getDomain(String domain) {
        return domainMapper.getDomain(domain);
    }
}

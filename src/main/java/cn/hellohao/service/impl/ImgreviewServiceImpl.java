package cn.hellohao.service.impl;

import cn.hellohao.mapper.ImgreviewMapper;
import cn.hellohao.entity.Imgreview;
import cn.hellohao.service.ImgreviewService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class ImgreviewServiceImpl implements ImgreviewService {

    @Resource
    private ImgreviewMapper imgreviewMapper;

    @Override
    public int deleteByPrimaryKey(Integer id) {
        return 0;
    }

    @Override
    public int insert(Imgreview record) {
        return 0;
    }

    @Override
    public int insertSelective(Imgreview record) {
        return 0;
    }

    @Override
    public Imgreview selectByPrimaryKey(Integer id) {
        return imgreviewMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(Imgreview record) {
        return imgreviewMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Imgreview record) {
        return 0;
    }
}

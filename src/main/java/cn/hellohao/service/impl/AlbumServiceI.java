package cn.hellohao.service.impl;

import cn.hellohao.mapper.AlbumMapper;
import cn.hellohao.mapper.ConfigMapper;
import cn.hellohao.mapper.ImgAndAlbumMapper;
import cn.hellohao.exception.CodeException;
import cn.hellohao.entity.Album;
import cn.hellohao.entity.ImgAndAlbum;
import cn.hellohao.service.AlbumService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author Hellohao
 * @version 1.0
 * @date 2019-12-18 22:30
 */
@Service
public class AlbumServiceI extends ServiceImpl<AlbumMapper,Album> implements AlbumService {
    @Resource
    AlbumMapper albumMapper;
    @Resource
    ImgAndAlbumMapper andAlbumMapper;
    @Resource
    ConfigMapper configMapper;

    @Override
    public Album selectAlbum(Album album) {
        return albumMapper.selectAlbum(album);
    }

    @Override
    public Integer addAlbum(Album album) {
        return albumMapper.addAlbum(album);
    }

    @Override
    @Transactional
    public Integer addAlbumForImgAndAlbumMapper(ImgAndAlbum imgAndAlbum) {
        Integer tem = 0;
        Integer r2 = andAlbumMapper.addImgAndAlbum(imgAndAlbum);
        if(r2>0){
            tem = 1;
        }else{
            throw new CodeException("插入画廊数据失败，回滚");
        }
        return tem;
    }

    @Override
    public Integer deleteAlbum(String albumkey) {
        return albumMapper.deleteAlbum(albumkey);
    }

    @Override
    public List<Album> selectAlbumURLList(Album album) {
        return albumMapper.selectAlbumURLList(album);
    }

    @Transactional
    public Integer delete(String albumkey) {
        Integer ret1 = albumMapper.deleteAlbum(albumkey);
        if(ret1>0){
            ret1 = andAlbumMapper.deleteImgAndAlbumForKey(albumkey);
        }else{
            throw new CodeException("删除画廊失败。");
        }
        return ret1;
    }

    @Transactional
    public Integer deleteAll(String[] albumkeyArr) {
        Integer ret1 = 0 ;
        for (String s : albumkeyArr) {
            ret1 = albumMapper.deleteAlbum(s);
            if(ret1>0){
                ret1 = andAlbumMapper.deleteImgAndAlbumForKey(s);
            }else{
                throw new CodeException("删除画廊失败。");
            }
        }
        return ret1;
    }
}

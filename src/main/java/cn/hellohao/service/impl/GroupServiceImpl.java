package cn.hellohao.service.impl;

import cn.hellohao.mapper.GroupMapper;
import cn.hellohao.mapper.UserMapper;
import cn.hellohao.exception.CodeException;
import cn.hellohao.entity.Group;
import cn.hellohao.entity.User;
import cn.hellohao.service.GroupService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author Hellohao
 * @version 1.0
 * @date 2019/8/19 16:30
 */
@Service
public class GroupServiceImpl extends ServiceImpl<GroupMapper,Group> implements GroupService {
    @Resource
    private GroupMapper groupMapper;
    @Resource
    private UserMapper userMapper;

    @Override
    public List<Group> grouplist() {
        return groupMapper.grouplist();
    }

    @Override
    public Group idgrouplist(Integer id) {
        return groupMapper.idgrouplist(id);
    }


    /**
     * 删除群组
     * @param id 群组id
     * @return 成功1:0
     */
    @Override
    @Transactional
    public Integer delegroup(Integer id) {
        int ret  ;
        ret = groupMapper.deleteById(id);
        if(ret>0 ){
            List<User> userList = userMapper.getuserlistforgroupid(id);
            for (User user : userList) {
                User u = new User();
                u.setGroupid(1);
                u.setUid(user.getUid());
                userMapper.change(u);
            }

        }else{
            throw new CodeException("用户之没有设置成功。");
        }
        return ret;
    }

}

package cn.hellohao.service.impl;

import cn.hellohao.mapper.UploadConfigMapper;
import cn.hellohao.entity.UploadConfig;
import cn.hellohao.service.UploadConfigService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class UploadConfigServiceImpl implements UploadConfigService {
    @Resource
    private UploadConfigMapper uploadConfigMapper;

    @Override
    public UploadConfig getUpdateConfig() {
        return uploadConfigMapper.getUpdateConfig();
    }

    @Override
    public Integer setUpdateConfig(UploadConfig uploadConfig) {
        return uploadConfigMapper.setUpdateConfig(uploadConfig);
    }
}


package cn.hellohao.service.impl;

import cn.hellohao.mapper.UserGroupMapper;
import cn.hellohao.entity.UserGroup;
import cn.hellohao.service.UserGroupService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author Hellohao
 * @version 1.0
 * @date 2019/8/20 14:13
 */
@Service
public class UserGroupServiceImpl extends ServiceImpl<UserGroupMapper,UserGroup> implements UserGroupService {
    @Resource
    private UserGroupMapper userGroupMapper;


    @Override
    public Integer updateusergroupdefault(Integer groupid) {
        return userGroupMapper.updateusergroupdefault(groupid);
    }

}

package cn.hellohao.service.impl;

import cn.hellohao.mapper.ConfigMapper;
import cn.hellohao.entity.Config;
import cn.hellohao.service.ConfigService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class ConfigServiceImpl extends ServiceImpl<ConfigMapper,Config> implements ConfigService {
    @Resource
    private ConfigMapper configMapper;
    @Override
    public Config getSourceype() {
        return configMapper.getSourceype();
    }

    @Override
    public Integer setSourceype(Config config) {
        return configMapper.setSourceype(config);
    }
}

package cn.hellohao.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import cn.hellohao.mapper.KeysMapper;
import cn.hellohao.entity.Keys;
import cn.hellohao.service.KeysService;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author yanni
 */
@Service
public class KeysServiceImpl extends ServiceImpl<KeysMapper,Keys> implements KeysService {

    @Resource
    private KeysMapper keysMapper;

    @Override
    public Keys selectKeys(Integer storageType) {
        // TODO Auto-generated method stub
        return keysMapper.selectKeys(storageType);
    }


    @Override
    public List<Keys> getKeys() {
        return null;
    }

}

package cn.hellohao.service.impl;

import cn.hellohao.mapper.EmailConfigMapper;
import cn.hellohao.entity.EmailConfig;
import cn.hellohao.service.EmailConfigService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author yanni
 */
@Service
public class EmailServiceImpl extends ServiceImpl<EmailConfigMapper,EmailConfig> implements EmailConfigService {
    @Resource
    EmailConfigMapper emailConfigMapper;
    @Override
    public EmailConfig getemail() {
        return emailConfigMapper.getemail();
    }


}

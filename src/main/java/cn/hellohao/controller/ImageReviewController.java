package cn.hellohao.controller;

import cn.hellohao.entity.Imgreview;
import cn.hellohao.service.ImgreviewService;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/admin")
public class ImageReviewController {

    @Resource
    private ImgreviewService imgreviewService;


    @GetMapping(value = "/root/ImageReview")
    public String forwardImageReview(Model model) {
        Imgreview imgreview = imgreviewService.selectByPrimaryKey(1);
        model.addAttribute("appid", imgreview.getAppId());
        model.addAttribute("apikey", imgreview.getApiKey());
        model.addAttribute("secretkey", imgreview.getSecretKey());
        model.addAttribute("using", imgreview.getUsing());
        return "admin/imageIdentify";
    }

    @GetMapping(value = "/root/ImgreviewSwitch")
    public String imgReviewSwitch(String appId, String apiKey, String secretKey, Integer using) {
        Imgreview imgreview = new Imgreview();
        //目前就一种鉴黄功能所以设死了
        imgreview.setId(1);
        imgreview.setUsing(using);
        imgreview.setAppId(appId);
        imgreview.setApiKey(apiKey);
        imgreview.setSecretKey(secretKey);
        int ret = imgreviewService.updateByPrimaryKeySelective(imgreview);

        return Integer.toString(ret);
    }

}

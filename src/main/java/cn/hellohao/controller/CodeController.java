package cn.hellohao.controller;

import cn.hellohao.entity.Code;
import cn.hellohao.entity.User;
import cn.hellohao.service.CodeService;
import cn.hutool.crypto.SecureUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.*;

/**
 * @author Hellohao
 * @version 1.0
 * @date 2019-08-11 14:25
 */
@Controller
@RequestMapping("/admin/root")
public class CodeController {
    @Resource
    private CodeService codeService;


    @GetMapping(value = "/tocode")
    public String tocode() {

        return "admin/code";
    }

    @PostMapping(value = "/selectcodelist")
    @ResponseBody
    public Map<String, Object> selectcodel(HttpSession session, @RequestParam(required = false, defaultValue = "1") int page,
                                            @RequestParam(required = false) int limit) {
        User u = (User) session.getAttribute("user");
        PageHelper.startPage(page, limit);
        List<Code> codes = null;
        if (u.getLevel() > 1) {
            codes = codeService.selectCode(null);
            // 使用pageInfo包装查询
            PageInfo<Code> rolePageInfo = new PageInfo<>(codes);
            Map<String, Object> map = new HashMap<>(4);
            map.put("code", 0);
            map.put("msg", "");
            map.put("count", rolePageInfo.getTotal());
            map.put("data", rolePageInfo.getList());
            return map;
        } else {
            return null;
        }
    }

    @DeleteMapping("/deletecodes")
    @ResponseBody
    public Integer deletecodes(HttpSession session, @RequestParam("arr[]") String[] arr){
        Integer v = 0;
        User u = (User) session.getAttribute("user");
        for (String s : arr) {
            v = codeService.deleteCode(s);
        }
        return v;
    }
    @DeleteMapping("/deletecode")
    @ResponseBody
    public Integer deletecode(String code){
        return codeService.deleteCode(code);
    }

    @PostMapping("/addcode")
    @ResponseBody
    public Integer addcode(Integer value,Integer counts){
        int val = 0;
        Code code = new Code();
        for (int i= 0;i<counts;i++) {
            java.text.DateFormat format1 = new java.text.SimpleDateFormat("hhmmss");
            Integer number = (int)(Math.random()*100000)+1;
            String uuid = UUID.randomUUID().toString().replace("-", "").toLowerCase().substring(0,5);
            code.setValue(value);
            code.setCode(SecureUtil.sha256(number+format1.format(new Date())+uuid));
            codeService.addCode(code);
            val = 1;
        }
        return val;
    }

}

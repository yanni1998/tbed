package cn.hellohao.controller;

import cn.hellohao.entity.Group;
import cn.hellohao.entity.Keys;
import cn.hellohao.entity.ResultBean;
import cn.hellohao.entity.User;
import cn.hellohao.service.GroupService;
import cn.hellohao.service.KeysService;
import cn.hellohao.service.UserService;
import cn.hellohao.utils.StringUtils;
import com.alibaba.fastjson.JSONArray;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Hellohao
 * @version 1.0
 * @date 2019/8/19 16:35
 */
@Controller
@RequestMapping("/admin/root")
public class GroupController {
    @Resource
    private GroupService groupService;
    @Resource
    private KeysService keysService;
    @Resource
    private UserService userService;


    @GetMapping(value = "/group")
    public String togroup() {
        return "admin/group";
    }
    @GetMapping(value = "/addgroup")
    public String addgroup() {
        return "admin/addgroup";
    }
    //获取code列表
    @GetMapping(value = "/getgrouplist")
    @ResponseBody
    public Map<String, Object> getgrouplist(HttpSession session, @RequestParam(required = false, defaultValue = "1") int page,
                                           @RequestParam(required = false) int limit) {
        User u = (User) session.getAttribute("user");
        PageHelper.startPage(page, limit);
        List<Group> group = null;
        if (u.getLevel() > 1) {
            group = groupService.grouplist();
            PageInfo<Group> rolePageInfo = new PageInfo<>(group);
            Map<String, Object> map = new HashMap<>(4);
            map.put("code", 0);
            map.put("msg", "");
            map.put("count", rolePageInfo.getTotal());
            map.put("data", rolePageInfo.getList());
            return map;
        } else {
            return null;
        }
    }

    @PostMapping(value = "/addisgroup")
    @ResponseBody
    public boolean addisgroup(@RequestBody  Group group) {
        boolean b;
        if(group.getKeyid()==5){
            b =true;
        }else{
            Keys key = keysService.selectKeys(group.getKeyid());
            b = StringUtils.doNull(group.getKeyid(),key);
        }
        boolean ret ;
        if(b){
            ret = groupService.save(group);
        }else{
            ret = false;
        }
        return ret;
    }
    @DeleteMapping(value = "/delegroup")
    @ResponseBody
    public Integer delGroup(Integer id) {
        int ret = 0;
        if(id >0){
            ret = groupService.delegroup(id);
        }
        return ret;
    }

    @PostMapping("/getgrouplist")
    @ResponseBody
    public String getgrouplist() {
        List<Group> li = groupService.grouplist();
        JSONArray jsonArray = new JSONArray();
        jsonArray.addAll(li);
        return jsonArray.toString();
    }

    @PostMapping("/updateuser")
    @ResponseBody
    public Integer updateuser(User user) {
        return userService.change(user);
    }

    @PostMapping("/updategroup")
    @ResponseBody
    public ResultBean updategroup(@RequestBody Group group) {
        boolean res=groupService.updateById(group);
        return ResultBean.success(res);
    }

    @GetMapping("/modifygroup")
    public String modifygroup(Model model, Integer id) {
        Group group = groupService.idgrouplist(id);
        model.addAttribute("group",group);
        return "admin/setgroup";
    }



}

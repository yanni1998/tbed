package cn.hellohao.mapper;

import cn.hellohao.entity.SysConfig;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

/**
 * @author Hellohao
 * @version 1.0
 * @date 2019/8/15 13:33
 */
@Mapper
public interface SysConfigMapper extends BaseMapper<SysConfig> {
    @Select("select * from sysconfig where id = 1")
    SysConfig getstate();
    Integer setstate(SysConfig sysConfig);
}

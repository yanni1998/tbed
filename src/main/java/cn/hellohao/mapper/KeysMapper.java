package cn.hellohao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import cn.hellohao.entity.Keys;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface KeysMapper extends BaseMapper<Keys> {
    //查询密钥
    @Select("select * from `keys` where storageType = #{storageType}")
    Keys selectKeys(@Param("storageType") Integer storageType);

    @Select("select * from `keys`")
    List<Keys> getKeys();

}

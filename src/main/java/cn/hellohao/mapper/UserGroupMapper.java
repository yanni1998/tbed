package cn.hellohao.mapper;

import cn.hellohao.entity.UserGroup;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * @author Hellohao
 * @version 1.0
 * @date 2019/8/20 13:45
 */
@Mapper
public interface UserGroupMapper extends BaseMapper<UserGroup> {

    @Select("select * from usergroup where id =#{id}")
    UserGroup idgetusergroup(@Param("id") Integer id);



}

package cn.hellohao.mapper;

import cn.hellohao.entity.EmailConfig;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface EmailConfigMapper extends BaseMapper<EmailConfig> {
    @Select("select * from emailconfig where id = 1")
    EmailConfig getemail();
}

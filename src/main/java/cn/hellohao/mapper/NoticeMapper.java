package cn.hellohao.mapper;

import cn.hellohao.entity.Notice;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

/**
 * @author yanni
 */
@Mapper
public interface NoticeMapper extends BaseMapper<Notice> {
@Select("select text from notice where id = 1")
    String getNotice();
}

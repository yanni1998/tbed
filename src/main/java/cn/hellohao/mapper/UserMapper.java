package cn.hellohao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import cn.hellohao.entity.Images;
import cn.hellohao.entity.User;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface UserMapper extends BaseMapper<User> {
    //注册

    //登录
    Integer login(@Param("email") String email, @Param("password") String password,@Param("uid") String uid);

    //获取用户信息
    User getUsers(@Param("email") String email);

    //插入图片
    Integer insertimg(Images img);

    //修改资料
    Integer change(User user);

    //检查用户名是否重复
    @Select("SELECT count(username) FROM `user` where username=#{username}")
    Integer checkUsername(@Param("username") String username);

    @Select("SELECT count(*) FROM `user`")
    Integer getUserTotal();

    List<User> getuserlist(User user);


    //查询用户名或者邮箱是否存在
    Integer countusername(@Param("username") String username);

    @Select("select COUNT(*)  from user  where email = #{email}")
    Integer countmail(@Param("email") String email);

    Integer uiduser(@Param("uid") String uid);

    User getUsersMail(@Param("uid") String uid);
    Integer setisok (User user);

    Integer setmemory(User user);
    User getUsersid(@Param("id") Integer id);

    List<User> getuserlistforgroupid(@Param("groupid") Integer groupid);

}

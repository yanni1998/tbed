package cn.hellohao.mapper;

import cn.hellohao.entity.Domain;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @author Hellohao
 * @version 1.0
 * @date 2019/8/21 9:46
 */
@Mapper
public interface DomainMapper extends BaseMapper<Domain> {
    Integer getDomain(@Param("domain") String domain);

}

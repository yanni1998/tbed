package cn.hellohao.mapper;

import cn.hellohao.entity.Group;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author Hellohao
 * @version 1.0
 * @date 2019/8/19 16:11
 */
@Mapper
public interface GroupMapper extends BaseMapper<Group> {
    @Select("select * from `group`")
    List<Group> grouplist();
    @Select("select * from `group` where id = #{id}")
    Group idgrouplist(@Param("id") Integer id);

    /**
     * 添加群组
     * @param group 群组对象
     * @return
     */
    Integer delGroup(@Param("id") Integer id);

}

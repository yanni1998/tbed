package cn.hellohao.utils;

import cn.hellohao.entity.Config;
import cn.hellohao.entity.EmailConfig;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.Properties;

public class SendEmail {

    public static MimeMessage Emails(EmailConfig emailConfig) {
        Properties p = new Properties();
        p.setProperty("mail.smtp.auth", "true");
        p.setProperty("mail.smtp.host", emailConfig.getEmailurl());
        p.setProperty("mail.smtp.port", emailConfig.getPort());
        p.setProperty("mail.smtp.socketFactory.port", emailConfig.getPort());
        p.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        //p.setProperty("mail.smtp.socketFactory.class", "SSL_FACTORY");

        Session session = Session.getInstance(p, new Authenticator() {
            // 设置认证账户信息
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(emailConfig.getEmails(), emailConfig.getEmailkey());
            }
        });
        session.setDebug(true);
        return new MimeMessage(session);
    }

    public static Integer sendEmail(MimeMessage message, String username, String Url, String email, EmailConfig emailConfig, Config config) {
        String webname=config.getWebname();
        String domain = config.getDomain();
        String link="<a href='"+domain+"/user/activation.do?activation=" + Url + "&username=" + username + "'>"+domain+"/user/activation.do?activation=" + Url + "&username=" + username + "</a>";
        String actLink="<a href='"+domain+"/user/activation.do?activation=" + Url + "&username=" + username + "'>"+ "激活链接" + "</a>";
        String texts = "<h3>你正在注册【"+webname+"】，点击下方链接进行激活：</h3><br />"+link;
        String body = "<!DOCTYPE html><html lang=\"zh-CN\"><head><meta charset=\"UTF-8\"><style type=\"text/css\">*{margin:0;padding:0;box-sizing:border-box}a{color:inherit;text-decoration:none;background-color:transparent}li{list-style:none}body{font-size:14px;color:#494949;overflow:auto}.wrap{position:relative;min-height:580px}.wrap-bg circle,.wrap-bg rect{stroke-width:0;-ms-transform:rotate(30deg)scale(1.1);transform:rotate(30deg)scale(1.1);-ms-transform-origin:center;transform-origin:center}.main{position:absolute;top:50%;left:50%;z-index:2;width:970px;background:#effbff;-ms-transform:translate(-50%,-50%);transform:translate(-50%,-50%);box-shadow:0 0 50px rgba(0,0,0,.1)}.header-tabs.tab span{float:left}.header-tabs.tab i{position:relative;float:right;width:11px;height:11px;margin-top:14px;cursor:pointer;-ms-transform:rotate(45deg);transform:rotate(45deg)}.header-tabs.tab i:before{content:'';position:absolute;top:5px;left:0;display:block;width:100%;height:1px;background:#113c73}.header-tabs.tab i:after{content:'';position:absolute;left:5px;top:0;display:block;width:1px;height:100%;background:#113c73}.header-tabs.tabs-tool a{position:relative;float:left;width:14px;height:100%;margin:0 15px}.header-url a{float:left;height:100%;margin:0 9px;overflow:hidden}.header-url a svg{margin-top:9px}.header-url.btn-refresh path{fill:none;stroke:#494949;stroke-width:2px;stroke-linecap:round;stroke-linejoin:round;-ms-transform-origin:center;transform-origin:center;-ms-transform:rotate(40deg);transform:rotate(40deg)}.header-url.btn-refresh polyline{fill:#494949;-ms-transform-origin:center;transform-origin:center;-ms-transform:rotate(40deg);transform:rotate(40deg)}.header-url input[type=text]{height:100%;width:820px;margin-left:6px;padding:0 1em;border:1px solid#a9a9a9;border-radius:4px;background:#fff}.main-content{height:470px;border:1px solid#c4dce5;background:#f4fcff}.main-content h5{padding:110px 0;font-weight:400;text-align:center}.main-content h5 span{position:relative;display:inline-block;font-size:60px;color:#2c7ce3}.main-content h5 span:before{content:'';position:absolute;top:48px;left:-36px;display:block;width:14px;height:14px;border-radius:50%;background:linear-gradient(45deg,#ded9ff,#2c7ce3);opacity:.2}.main-content h5 span:after{content:'';position:absolute;top:32px;left:-80px;display:block;width:20px;height:20px;border-radius:50%;background:linear-gradient(45deg,#ded9ff,#2c7ce3);opacity:.2}.main-content h5 span i:before{content:'';position:absolute;top:32px;right:-72px;display:block;width:14px;height:14px;border-radius:50%;background:linear-gradient(45deg,#ded9ff,#2c7ce3);opacity:.2}.main-content h5 span i:after{content:'';position:absolute;top:48px;right:-40px;display:block;width:20px;height:20px;border-radius:50%;background:linear-gradient(45deg,#ded9ff,#2c7ce3);opacity:.2}.main-content p{font-size:26px;text-align:center}</style><title></title></head><body><div class=\"wrap\"><div class=\"main\"><div class=\"main-content\"><h5><span>您正在注册"+webname+"<i></i></span></h5><p>点击"+actLink+"进行账号激活</p></div></div></div></body></html>";
        try {
            // 发件人
            message.setFrom(new InternetAddress(emailConfig.getEmails(), emailConfig.getEmailname(), "UTF-8"));
            // 收件人和抄送人
            message.setRecipients(Message.RecipientType.TO, email);
            message.setSubject(emailConfig.getEmailname()+"账号激活");
            message.setContent(body, "text/html;charset=UTF-8");
            message.setSentDate(new Date());
            message.saveChanges();
            Transport.send(message);
            return 1;
        } catch (MessagingException | UnsupportedEncodingException e) {
            e.printStackTrace();
            return 0;
        }
    }


}

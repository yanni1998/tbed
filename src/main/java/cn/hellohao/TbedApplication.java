package cn.hellohao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.util.unit.DataSize;
import springfox.documentation.oas.annotations.EnableOpenApi;

import javax.servlet.MultipartConfigElement;

@SpringBootApplication
@Configuration
@EnableOpenApi
@EnableConfigurationProperties
@EnableTransactionManagement(proxyTargetClass = true)
public class TbedApplication {

    //public void customize(UndertowServletWebServerFactory factory) {
    //    factory.addBuilderCustomizers(builder -> {
    //        builder.addHttpListener(8088, "0.0.0.0");
    //    });
    //}
public static void main(String[] args) {
    SpringApplication.run(TbedApplication.class, args);
    }
    /**
     * 文件上传配置
     *
     * @return
     */
    @Bean
    public MultipartConfigElement multipartConfigElement() {
        MultipartConfigFactory factory = new MultipartConfigFactory();
        //  单个数据大小
        factory.setMaxFileSize(DataSize.parse("102400KB")); // KB,MB
        /// 总上传数据大小
        factory.setMaxRequestSize(DataSize.parse("102400KB"));
        //factory.setLocation("/tmp");

        return factory.createMultipartConfig();
    }

}

